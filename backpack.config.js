const path = require('path')

module.exports = {
  webpack: (config, options, webpack) => {
    config.entry.main = './index.js'
    config.resolve.alias = {
      '@': path.resolve(__dirname),
    }
    return config
  }
}
